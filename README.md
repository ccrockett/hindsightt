# README
[![pipeline status](https://gitlab.com/ccrockett/hindsightt/badges/master/pipeline.svg)](https://gitlab.com/ccrockett/hindsightt/commits/master) [![coverage report](https://gitlab.com/ccrockett/hindsightt/badges/master/coverage.svg)](https://gitlab.com/ccrockett/hindsightt/commits/master)
# Getting Started

#### What is Hindsightt
- It's a platform to track your personal data so you (the user) can query past "events"
- How many times have you asked yourself one of the following questions:
  * Did I take my medicine this morning?
  * How many drinks did I have last night?
  * How many miles did I run this week? or last week? or last month?
- Hindsightt app is here to answer all those questions. Just send it the data and then ask Hindsightt the those questions to find out

#### Examples using a Hindsightt Bot (ie: texting or Telegram App)
- Create an entry that you had a beer with the size, the type<br />
`health beer type,budlight,size,12oz`
 
 - Create an entry that you ran with the distance<br />
`health ran distance,1mi`
 
- Calculate how long it takes to get to work
    1. Create an entry before going to work <br />
    `work transit start`
    2. Do it again when you get to work <br />
    `work transit stop`

#### Start your own site
* To be Updated


#### Running in Development
1. `docker run -p 9200:9200 -p 9300:9300 elasticsearch:latest`
1. Update config/hindsightt.yml with your service (twilio, telegram or plivo) secrets
1. `./ngrok http 8080`
1. Update service to ngrok url
