# frozen_string_literal: true

module HeaderHelpers
  def plivo_headers(nonce, uri, account_token)
    request = {
      "HTTP_X_PLIVO_SIGNATURE" => "JoNFdNH0teLmrncEeXBxGz+Z17k=",
      "HTTP_X_PLIVO_SIGNATURE_V2_NONCE" => nonce,
      "REQUEST_URI" => uri  
    }

    parsed_uri = URI.parse(uri)
    uri_details = { host: parsed_uri.host, path: parsed_uri.path }
    uri_builder_module = parsed_uri.scheme == 'https' ? URI::HTTPS : URI::HTTP
    data_to_sign = uri_builder_module.build(uri_details).to_s + nonce
    sha256_digest = OpenSSL::Digest.new('sha256')
    request["HTTP_X_PLIVO_SIGNATURE_V2"] = Base64.encode64(OpenSSL::HMAC.digest(sha256_digest, account_token, data_to_sign)).strip()
    request
  end
end