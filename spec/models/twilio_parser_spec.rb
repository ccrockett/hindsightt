# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TwilioParser, type: :model do
  account_sid = SecureRandom.hex
  to_number = '+15127777777'
  from_number = '+15129999999'

  let(:twilio_parser) { TwilioParser.new }

  before(:each) do
    clear_all_indices
  end

  context 'in events' do
    it 'should return successful' do
      user = add_user_to_es
      parser = twilio_parser
      msg = twilio_msg(to_number, from_number, account_sid, 'health drink type,sparkling.ice')
      expect(parser).to receive(:send_response).with(from_number, I18n.t('event_successful'))
      response = parser.parse(msg, nil)
      expect(User.find_by_phone(from_number).reference_id).to eq(user.reference_id)
    end

    it 'should return instructions to signup' do
      parser = twilio_parser
      msg = twilio_msg(to_number, from_number, account_sid, 'health drink type,sparkling.ice')
      expect(parser).to receive(:send_response).with(from_number, I18n.t('errors.user_not_found')+I18n.t('instructions.user_not_found'))
      parser.parse(msg)
    end

    it 'should signup user with phone number' do
      parser = twilio_parser
      msg = twilio_msg(to_number, from_number, account_sid, '/signup test.user')
      expect(parser).to receive(:send_response)
      parser.parse(msg)
      HindsighttRepository.client.indices.flush
      expect(User.find_by_phone(from_number)).to be_a(User)
    end
  end
end
