# frozen_string_literal: true

require 'rails_helper'

RSpec.describe EventMessage, type: :model do
  before(:each) do    
    clear_all_indices
    @from = '+15129999999'
  end

  context 'in events' do
    it 'should return no user' do
      message = 'health drink type, sparkling.ice'
      user = User.find_by_phone(@from)
      message = EventMessage.new(message, user)
      response = message.execute
      expect(response.error[:message]).to eq('Could not find user')
    end

    it 'should parse periods as spaces in fields' do
      add_user_to_es
      message = 'health drink type, sparkling.ice'
      user = User.find_by_phone(@from)
      message = EventMessage.new(message, user)
      response = message.execute
      expect(response.fields['type']).to eq('sparkling ice')
    end

    it 'should parse hyphens as spaces in fields' do
      add_user_to_es
      message = 'health drink type,sparkling-ice'
      user = User.find_by_phone(@from)
      message = EventMessage.new(message, user)
      response = message.execute
      expect(response.fields['type']).to eq('sparkling ice')
    end

    it 'should parse spaced terms as tags' do
      add_user_to_es
      message = 'health drink type,sparkling-ice'
      user = User.find_by_phone(@from)
      message = EventMessage.new(message, user)
      response = message.execute
      expect(response.tags).to eq(%w[health drink])
    end

    it 'should parse terms with comma as fields' do
      add_user_to_es
      message = 'health drink type, sparkling-ice'
      user = User.find_by_phone(@from)
      message = EventMessage.new(message, user)
      response = message.execute
      expect(response.fields['type']).to eq('sparkling ice')
    end

    it 'should parse multiple terms with comma as fields' do
      add_user_to_es
      message = 'health drink type, sparkling-ice, size,medium'
      user = User.find_by_phone(@from)
      message = EventMessage.new(message, user)
      response = message.execute
      expect(response.fields['type']).to eq('sparkling ice')
      expect(response.fields['size']).to eq('medium')
    end

    it 'should parse periods as spaces in tags' do
      add_user_to_es
      message = 'health drink kashi.go.lean.original type, sparkling.ice'
      user = User.find_by_phone(@from)
      message = EventMessage.new(message, user)
      response = message.execute
      expect(response.tags).to eq(["health", "drink", "kashi go lean original"])
    end

    it 'should parse hyphens as spaces in tags' do
      add_user_to_es
      message = 'health drink kashi-go-lean-original type, sparkling.ice'
      user = User.find_by_phone(@from)
      message = EventMessage.new(message, user)
      response = message.execute
      expect(response.tags).to eq(["health", "drink", "kashi go lean original"])
    end
  end
end
