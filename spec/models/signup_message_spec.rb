# frozen_string_literal: true

require 'rails_helper'

RSpec.describe SignupMessage, type: :model do
  before(:each) do    
    clear_all_indices
  end

  context 'in user commands' do
    it 'should create a new user' do
      message = '/signup ccrockett'
      message = SignupMessage.new(message, nil)
      response = message.execute
      expect(response.username).to eq('ccrockett')
    end

    it 'should NOT allow duplicate users' do
      add_user_to_es
      message = '/signup test.user'
      message = SignupMessage.new(message, nil)
      response = message.execute
      expect(response.error[:message]).to eq('username already taken')
    end

    it 'should NOT allow user without username' do
      message = '/signup'
      message = SignupMessage.new(message, nil)
      response = message.execute
      expect(response.error[:message]).to eq('username can\'t be empty')
    end
  end
end
