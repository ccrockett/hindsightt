# frozen_string_literal: true

require('rails_helper')

RSpec.describe PlivoParser, type: :model do
  account_token = SecureRandom.hex[0,9]
  uri = "https://fa207fa6.ngrok.io/message/sms"
  nonce = "73930157863581074210"

  let(:plivo_parser) { 
    allow_any_instance_of(PlivoParser).to receive(:valid?).with(any_args).and_return(true)
    PlivoParser.new 
  }

  before(:each) do
    clear_all_indices
  end

  context 'in events' do
    it 'should return successful' do
      add_user_to_es
      request = plivo_headers(nonce, uri, account_token)
      parser = plivo_parser
      msg = plivo_msg(account_token: account_token, text:'health drink type,sparkling.ice')
      expect(parser).to receive(:send_response).with('15129999999', I18n.t('event_successful'))
      parser.parse(msg, request)
    end

    it 'should return instructions to signup' do
      request = plivo_headers(nonce, uri, account_token)
      parser = plivo_parser
      msg = plivo_msg(account_token: account_token, text:'health drink type,sparkling.ice')
      expect(parser).to receive(:send_response).with('15129999999', I18n.t('errors.user_not_found')+I18n.t('instructions.user_not_found'))
      parser.parse(msg, request)
    end

    it 'should signup user with phone number' do
      from_number = '15129999999'
      request = plivo_headers(nonce, uri, account_token)
      parser = plivo_parser
      msg = plivo_msg(phone_number: from_number, account_token: account_token, text:'/signup test.user')
      expect(parser).to receive(:send_response)
      parser.parse(msg, request)
      HindsighttRepository.client.indices.flush
      expect(User.find_by_phone(from_number)).to be_a(User)
    end

  end
end
