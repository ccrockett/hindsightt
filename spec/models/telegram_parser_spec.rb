# frozen_string_literal: true

require 'rails_helper'

RSpec.describe TelegramParser, type: :model do
  
  let(:telegram_parser) { TelegramParser.new }

  before(:each) do
    clear_all_indices
  end

  context 'in events' do
    it 'should return successful' do
      parser = telegram_parser
      msg = telegram_msg('health drink type,sparkling.ice')
      user_id = msg["message"]["from"]["id"]
      add_user_to_es(attributes: {account_id: user_id})
      expect(parser).to receive(:send_response).with(user_id, I18n.t('event_successful'))
      parser.parse(msg)
    end

    it 'should return false when no text' do
      parser = telegram_parser
      msg = telegram_msg('')
      user_id = msg["message"]["from"]["id"]
      add_user_to_es(attributes: {account_id: user_id})
      response = parser.parse(msg)
      expect(response).to be(false)
    end

    it 'should return false when not valid' do
      parser = telegram_parser
      msg = telegram_msg('health drink type,sparkling.ice')
      user_id = msg["message"]["from"]["id"]
      add_user_to_es(attributes: {account_id: user_id})
      msg["message"].delete("text")
      msg["parse"].delete("text")
      response = parser.parse(msg)
      expect(response).to be(false)
    end

    it 'should return instructions to signup' do
      parser = telegram_parser
      msg = telegram_msg('health drink type,sparkling.ice')
      user_id = msg["message"]["from"]["id"]
      expect(parser).to receive(:send_response).with(user_id, I18n.t('errors.user_not_found')+I18n.t('instructions.user_not_found'))
      parser.parse(msg)
    end

    it 'should ask user to share contact details if user not found' do
      parser = telegram_parser
      msg = telegram_msg('health drink type,sparkling.ice')
      user_id = msg["message"]["from"]["id"]
      expect(parser).to receive(:request_phone_num).with(user_id)
      parser.parse(msg)
    end

    it 'should ask user to share contact details if user exists but not connected' do
      parser = telegram_parser
      msg = telegram_msg('health drink type,sparkling.ice')
      user_id = msg["message"]["from"]["id"]
      expect(parser).to receive(:request_phone_num).with(user_id)
      parser.parse(msg)
    end

    it 'should signup user with phone number' do
      parser = telegram_parser
      msg = telegram_msg('/signup test.user')
      user_id = msg["message"]["from"]["id"]
      expect(parser).to receive(:send_response)
      parser.parse(msg)
      HindsighttRepository.client.indices.flush
      expect(User.find_by_service_acct(account_id: user_id)).to be_a(User)
    end

    it 'should connect telegram account to existing user' do
      test_phone_number = "1512342838"
      
      parser = telegram_parser
      msg = telegram_msg('phone number is needed for setup')
      user_id = msg["message"]["from"]["id"].to_i
      user = add_user_to_es(attributes: {account_id: nil, phone_number: "+#{test_phone_number}"})
      msg["message"]["contact"] = {"phone_number"=>test_phone_number, 
        "first_name"=>"Test", "last_name"=>"C", "user_id"=>user_id}
      expect(parser).to receive(:send_response).with(user_id, I18n.t('user_connected'))
      parser.parse(msg)
      HindsighttRepository.client.indices.flush
      expect(User.find_by_service_acct(account_id: user_id)).to be_a(User)
    end

    it 'should add a new user when phone number is sent' do
      test_phone_number = "1512342838"
      parser = telegram_parser
      msg = telegram_msg('phone number is needed for setup')
      user_id = msg["message"]["from"]["id"]
      msg["message"]["contact"] = {"phone_number"=>test_phone_number, 
        "first_name"=>"Test", "last_name"=>"C", "user_id"=>user_id}
      expect(parser).to receive(:send_response).with(user_id, I18n.t('user_successful'))
      parser.parse(msg)
      HindsighttRepository.client.indices.flush
      expect(User.find_by_service_acct(account_id: user_id)).to be_a(User)
    end

    it 'should return successful when unknown command' do
      # return true so that Telegram won't keep retrying
      parser = telegram_parser
      msg = telegram_msg('/start')
      user_id = msg["message"]["from"]["id"]
      add_user_to_es(attributes: {account_id: user_id})
      response = parser.parse(msg)
      expect(response).to be(true)
    end
  end
end
