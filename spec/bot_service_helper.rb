# frozen_string_literal: true

module BotServiceHelpers
  def telegram_msg(text)
    user_id = SecureRandom.hex.gsub(/[a-f]/,"")[0,9]
    update_id = SecureRandom.hex.gsub(/[a-f]/,"")[0,9]
    message_id = rand(1000)
    msg_len = text.length
    {
      "update_id"=> update_id, 
      "message"=>{
        "message_id"=> message_id, 
        "from"=>{"id"=>user_id.to_i, "is_bot"=>false, "first_name"=>"Test", "last_name"=>"C", "language_code"=>"en-us"}, 
        "chat"=>{"id"=>user_id.to_i, "first_name"=>"Test", "last_name"=>"C", "type"=>"private"}, 
        "date"=>Time.now.to_i, 
        "text"=>text, 
        "entities"=>[{"offset"=>0, "length"=>msg_len, "type"=>"bot_command"}]
      }, 
      "parse"=>{
        "update_id"=>update_id, 
        "message"=>{
          "message_id"=>message_id, 
          "from"=>{"id"=>user_id.to_i, "is_bot"=>false,"first_name"=>"Test", "last_name"=>"C", "language_code"=>"en-us"}, 
          "chat"=>{"id"=>user_id.to_i, "first_name"=>"Test", "last_name"=>"C", "type"=>"private"}, 
          "date"=>Time.now.to_i, "text"=>text, "entities"=>[{"offset"=>0, "length"=>msg_len, "type"=>"bot_command"}]
        }
      }
    }
  end

  def twilio_msg(to_number, from_number, account_sid, text)
    Rails.configuration.hindsightt['twilio_account_sid'] = account_sid    
    {
      'ToCountry'=> 'US', 'ToState'=> 'TX', 'SmsMessageSid'=> 'TE98f7161713754398b06f193af805e21c',
      'NumMedia'=> '0', 'ToCity'=> 'Austin', 'FromZip'=> '78701', 'SmsSid'=> 'TE98f7161713754398b06f193af805e21c',
      'FromState'=> 'TX', 'SmsStatus'=> 'received', 'FromCity'=> 'Austin', 'FromCountry'=> 'US',
      'To'=> to_number, 'ToZip'=> '', 'NumSegments'=> '1',
      'MessageSid'=> 'TE98f7161713754398b06f193af805e21c', 'AccountSid'=> account_sid,
      'From'=> from_number, 'ApiVersion'=> '2010-04-01', 'Body' => text
    }
  end

  def plivo_msg(attributes)
    attributes[:from_number] = attributes[:from_number] || '15129999999'
    attributes[:text] = attributes[:text] || ''

    Rails.configuration.hindsightt['plivo_auth_token'] = attributes[:account_token]
    {
      "From" => attributes[:from_number],
      "MessageUUID" => SecureRandom.uuid,
      "Text" => attributes[:text],
      "To" => "15127777777",
      "TotalAmount" => 0,
      "TotalRate" => 0,
      "Type" => "sms",
      "Units" => 1
    }
  end

  def plivo_signature(uri, nonce, auth_token)
    parsed_uri = URI.parse(uri)
    uri_details = { host: parsed_uri.host, path: parsed_uri.path }
    uri_builder_module = parsed_uri.scheme == 'https' ? URI::HTTPS : URI::HTTP
    data_to_sign = uri_builder_module.build(uri_details).to_s + nonce
    sha256_digest = OpenSSL::Digest.new('sha256')
    Base64.encode64(OpenSSL::HMAC.digest(sha256_digest, auth_token, data_to_sign)).strip()
  end
end
