# frozen_string_literal: true

module EsHelpers
  def clear_all_indices
    HindsighttRepository.client.indices.delete index: '_all'
  end

  def add_user_to_es(attributes: nil)
    attrib = attributes || {}
    attrib[:username] = 'test.user' unless attrib.has_key?(:username)
    attrib[:phone_number] = '+15129999999' unless attrib.has_key?(:phone_number)
    attrib[:account_id] = '555555555' unless attrib.has_key?(:account_id)
    user = User.new(attrib)
    user.save
    HindsighttRepository.client.indices.refresh index: 'users'
    return user
  end
end
