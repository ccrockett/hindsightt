# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ParseController, type: :controller do
  uniq_url = SecureRandom.hex
  Rails.configuration.hindsightt['url_unique'] = uniq_url

  before(:each) do
    clear_all_indices
  end

  it 'should return not found when no params' do
    params = {
      provider_name: "telegram",
      url_uuid: uniq_url
    }
    post :create, :params => params
    expect(response.status).to eq(404)
  end

  it 'telegram parser should return successful when command not known' do
    params = {
      provider_name: "telegram",
      url_uuid: uniq_url
    }
    params.merge!(telegram_msg('/start'))
    post :create, :params => params
    expect(response.status).to eq(200)
  end

  it 'plivo parser should return not found when command not known' do
    account_token = SecureRandom.hex
    nonce = "73930157863581074210"
    uri = "http://test.host/messages/provider/plivo/" + uniq_url
    @request.headers['REQUEST_URI'] = uri
    @request.headers['HTTP_X_PLIVO_SIGNATURE_V2_NONCE'] = nonce
    @request.headers['HTTP_X_PLIVO_SIGNATURE_V2'] = plivo_signature(uri, nonce, account_token)
    add_user_to_es

    allow_any_instance_of(PlivoParser).to receive(:send_response).with(any_args).and_return(nil)
    params = {
      provider_name: "plivo",
      url_uuid: uniq_url
    }
    params.merge!(plivo_msg(account_token: account_token, text:'/start'))
    post :create, :params => params
    expect(response.status).to eq(404)
  end

  it 'telegram parser should return successful' do
    add_user_to_es
    allow_any_instance_of(TelegramParser).to receive(:send_response).with(any_args).and_return(nil)
    params = {
      provider_name: "telegram",
      url_uuid: uniq_url
    }
    params.merge!(telegram_msg('health drink type,sparkling.ice'))
    post :create, :params => params
    expect(response.status).to eq(200)
  end

  it 'plivo parser should return successful' do
    account_token = SecureRandom.hex
    nonce = "73930157863581074210"
    uri = "http://test.host/messages/provider/plivo/" + uniq_url
    @request.headers['REQUEST_URI'] = uri
    @request.headers['HTTP_X_PLIVO_SIGNATURE_V2_NONCE'] = nonce
    @request.headers['HTTP_X_PLIVO_SIGNATURE_V2'] = plivo_signature(uri, nonce, account_token)
    add_user_to_es

    allow_any_instance_of(PlivoParser).to receive(:send_response).with(any_args).and_return(nil)
    params = {
      provider_name: "plivo",
      url_uuid: uniq_url
    }
    params.merge!(plivo_msg(account_token: account_token, text:'health drink type,sparkling.ice'))
    post :create, :params => params
    expect(response.status).to eq(200)
  end

  it 'twilio parser should return successful' do
    account_sid = SecureRandom.hex
    Rails.configuration.hindsightt['twilio_account_sid'] = account_sid
    add_user_to_es
    to_number = '+15127777777'
    from_number = '+15129999999'
    allow_any_instance_of(TwilioParser).to receive(:send_response).with(any_args).and_return(nil)
    params = {
      provider_name: "twilio",
      url_uuid: uniq_url
    }
    params.merge!(twilio_msg(to_number, from_number, account_sid, 'health drink type,sparkling.ice'))
    post :create, :params => params
    expect(response.status).to eq(200)
  end
end