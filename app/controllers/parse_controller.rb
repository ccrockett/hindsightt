# frozen_string_literal: true

class ParseController < ApplicationController
  def create
    provider_name = params[:provider_name]
    url_key = params[:url_uuid]
    Rails.logger.debug("Received message from provider: #{provider_name} with unique key: #{url_key}")
    parser = MessageParserFactory.create(provider_name)
    if (parser.parse(params, request))
      return render(status: :ok)
    end
    Rails.logger.debug("Failed to parse message")
    return render(status: :not_found)
  end
end
