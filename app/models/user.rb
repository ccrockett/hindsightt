# frozen_string_literal: true

require('securerandom')

class User
  attr_accessor :phone_number
  attr_accessor :account_id
  attr_reader :username
  attr_reader :reference_id
  attr_reader :error
  attr_reader :valid
  attr_accessor :id

  def initialize(attributes)
    phone_number = attributes[:phone_number] || attributes['phone_number'] || ''
    @phone_number = phone_number.start_with?("+") ? phone_number : "+#{phone_number}"
    @account_id = attributes[:account_id] || attributes['account_id'] || ''
    @username = attributes[:username] || attributes['username'] || ''
    @reference_id = attributes['reference_id'] || SecureRandom.uuid.delete('-')
  end

  def save
    if valid?(false)
      repository = init_index
      response = repository.save(self)
      @id = response["_id"]
      @valid = true
    else
      @valid = false
      @error = { message: @err_message }
    end
    self
  end

  def update(attributes)
    if valid?(true)
      source = attributes.map {|key,val| "ctx._source.#{key} = #{val}"}
      repository = init_index 
      repo_response = repository.update(@id, {type: 'user', script: {
        source: source.join(",")
      }})
      @valid = true
    else
      @valid = false
      @error = { message: @err_message }
    end
    self
  end

  def to_hash
    {
      account_id: @account_id,
      phone_number: @phone_number,
      username: @username,
      reference_id: @reference_id
    }
  end

  def self.find_by_phone(phone_number)
    repository = HindsighttRepository.new
    repository.index = 'users'
    return unless repository.client.indices.exists(index: 'users')
    search_number = phone_number.start_with?("+") ? phone_number : "+#{phone_number}"
    response = repository.search(query: { match: { phone_number: search_number } })
    user = nil
    if response.results.count == 1
      user = response.results.first
      user.id = response.response["hits"]["hits"].first._id
    end
    user
  end

  def self.find_by_service_acct(matcher)
    repository = HindsighttRepository.new
    repository.index = 'users'
    return unless repository.client.indices.exists(index: 'users')
    response = repository.search(query: { match: matcher })
    user = nil
    if response.results.count == 1
      user = response.results.first
      user.id = response.response["hits"]["hits"].first._id
    end
    user
  end

  private

  def init_index
    repository = HindsighttRepository.new
    repository.index = 'users'
    repository
  end

  def valid?(update_user)
    repository = init_index
    resp_length = 0
    if repository.client.indices.exists(index: 'users') && !@username.empty?
      response = repository.search(query: { match: { username: @username } })
      resp_length = response.results.length
    end
    
    @err_message = I18n.t('errors.empty_username') if @username.empty?
    @err_message = I18n.t('errors.username_taken') if !update_user && resp_length.positive?

    @err_message.nil?
  end
end
