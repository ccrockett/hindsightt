# frozen_string_literal: true

require 'uri'
require 'json'
require 'excon'

class TelegramParser < MessageParser
  def parse(msg, _request = nil)
    return false unless valid?(msg)
    account_id = msg['message']['from']['id']
    user = User.find_by_service_acct(account_id: account_id)
    @from = user&.account_id || account_id
    @phone_number = get_contact_info(msg)
    Rails.logger.debug("telegram message has phone_number: #{@phone_number}")
    # if contact details are in the message then
    # connect existing user or add new user
    if (@phone_number)
      msg = add_or_connect_user
      result = msg
      Rails.logger.debug("Respond with: #{msg}")
    else
      msg_parser = MessageFactory.create(user, msg['message']['text'], self)
      return true if msg_parser.nil?
      Rails.logger.debug("Received message from: #{@from}")
      result = msg_parser.execute
      msg = get_message(result)
    end
    send_response(@from, msg)
    result
  end

  def user_details
    details = {account_id: @from}
    if !@phone_number.nil?
      details[:phone_number] = @phone_number
    end
    details
  end

  private

  def send_response(from, msg)
    uri = "https://api.telegram.org"
    path = "bot#{Rails.configuration.hindsightt['telegram_token']}/sendMessage"
    if (msg == I18n.t('errors.user_not_found') + I18n.t('instructions.user_not_found'))
      msg_body = request_phone_num(from).to_json
    else
      msg_body = {
        "chat_id" => from,
        "text" => msg
      }.to_json
    end
    connection = Excon.new(uri,
      :body => msg_body,
      :headers => { "Content-Type" => "application/json" })
    post_response = connection.post(:path => path)
    Rails.logger.info("Received from telegram: #{post_response.inspect}")
  end

  def valid?(msg)
    has_id = !msg.dig('message','from','id').nil?
    has_text = !(msg.dig('message','text').nil? || msg['message']['text'].empty?) ||
      !(msg.dig('message','reply_to_message','text').nil? || msg['message']['reply_to_message']['text'].empty?)
    is_valid = has_id && has_text
    Rails.logger.debug("telegram message has_id: #{has_id}")
    Rails.logger.debug("telegram message has_text: #{has_text}")
    Rails.logger.debug("Valid telegram message: #{is_valid}")
    is_valid
  end

  def request_phone_num(chat_id)
    {
      "chat_id" => chat_id,
      "text" => I18n.t('telegram.phone_request'),
      "reply_markup" =>  {
        "keyboard" =>  [
          [
            {
              "text" => I18n.t('telegram.phone_req_button_text'),
              "request_contact" =>  true,
              "resize_keyboard" =>  true
            }
            ]
          ],
        "one_time_keyboard" =>  true
      }
    }
  end

  def get_contact_info(msg)
    has_contact = !(msg.dig('message','contact').nil? || msg['message']['contact']['phone_number'].empty?)
    if has_contact
      return msg['message']['contact']['phone_number']
    end
    nil
  end
 
  def add_or_connect_user
    user = User.find_by_phone(@phone_number)
    if user.nil?
      user = User.new(username: SecureRandom.hex, phone_number: @phone_number, account_id: @from)
      msg = I18n.t('user_successful')
      user.save
    else
      user.update({account_id: @from})
      msg = I18n.t('user_connected')
    end
    msg
  end
end
