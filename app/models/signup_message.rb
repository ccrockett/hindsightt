class SignupMessage
  attr_reader :from
  attr_reader :username
  attr_reader :tags
  attr_reader :fields
  attr_reader :command

  def initialize(message, parser_obj)
    # Request Body parsing
    @parser = parser_obj
    body = message.strip.gsub(/,\s+/, ',')
    msg_split = body.split(' ')
    @username = get_username(msg_split)
  end

  def execute
    Rails.logger.debug('Signing up user')
    attributes = @parser.nil? ? {} : @parser.user_details
    attributes[:username] = @username
    user = User.new(attributes)
    user.save
  end

  private
    def get_username(msg_split)
      return msg_split[1] if msg_split.length == 2
      nil
    end
end