class MessageFactory
  def self.create(user, message, parser_obj)
    msg = message.strip
    if (msg.start_with?('/'))
      return SignupMessage.new(msg, parser_obj) if (msg.start_with?('/signup'))
    else
      return EventMessage.new(msg, user)
    end
    Rails.logger.debug("MessageFactory failed to create object for #{msg}")
    return nil
  end
end