# frozen_string_literal: true

require('twilio-ruby')
class TwilioParser < MessageParser
  def parse(msg, request = nil)
    return false unless valid?(msg)
    phone_number = msg['From'].strip
    user = User.find_by_phone(phone_number)
    @from = user&.phone_number || phone_number
    msg_parser = MessageFactory.create(user, msg['Body'], self)
    return false if msg_parser.nil?
    Rails.logger.debug("Received message from: #{@from}")
    result = msg_parser.execute
    msg = get_message(result)
    send_response(@from, msg)
    result
  end

  def user_details
    {phone_number: @from}
  end

  private

  def send_response(from, msg)
    account_sid = Rails.configuration.hindsightt['twilio_account_sid']
    auth_token = Rails.configuration.hindsightt['twilio_auth_token']
    client = Twilio::REST::Client.new(account_sid, auth_token)
    client.api.account.messages.create(
      from: Rails.configuration.hindsightt['twilio_phone_num'],
      to: from,
      body: msg
    )
  end

  def valid?(msg)
    is_nil = !msg['AccountSid'].nil?
    is_valid_acct_sid = msg['AccountSid'] == Rails.configuration.hindsightt['twilio_account_sid']
    is_nil && is_valid_acct_sid
  end
end
