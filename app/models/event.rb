# frozen_string_literal: true

class Event
  attr_accessor :user
  attr_accessor :fields
  attr_accessor :tags
  attr_reader :type
  attr_reader :error
  attr_reader :valid

  def initialize(user: nil)
    @user = user
    @type = 'event'
  end

  def save
    if valid?
      repository = HindsighttRepository.new
      repository.index = "#{@user.reference_id}_#{Time.now.getutc.year}"
      Rails.logger.debug("Setting index to #{repository.index}")
      repository.save(self)
      @valid = true
    else
      @valid = false
      @error = { message: err_message }
    end
    self
  end

  def to_hash
    es_fields = @fields
    es_fields['tags'] = @tags
    es_fields['message_type'] = @type
    es_fields['timestamp'] = Time.now.to_i
    es_fields['user_id'] = @user.reference_id
    es_fields
  end

  private

  def valid?
    !@user.nil?
  end

  def err_message
    return I18n.t('errors.user_not_found') if @user.nil?
  end
end
