# frozen_string_literal: true

class MessageParser
  def get_message(result)
    result.valid ? I18n.t('event_successful') : get_error_msg(result.error[:message])
  end

  def get_error_msg(error_msg)
    Rails.logger.debug("Error occurred: #{error_msg}")
    error_msg += I18n.t('instructions.user_not_found') if error_msg == I18n.t('errors.user_not_found')
    error_msg
  end
end
