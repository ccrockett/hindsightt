# frozen_string_literal: true

class MessageParserFactory
  def self.create(type)
    case type
    when 'twilio'
      TwilioParser.new
    when 'telegram'
      TelegramParser.new
    when 'plivo'
      PlivoParser.new
    else
      raise('Unsupported type of parser')
    end
  end
end
