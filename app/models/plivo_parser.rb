# frozen_string_literal: true

require('plivo')
class PlivoParser < MessageParser
  include Plivo
  include Plivo::Exceptions

  def parse(msg, request)
    return false unless valid?(request)
    phone_number = "+#{msg['From'].strip}"
    user = User.find_by_phone(phone_number)
    @from = user&.phone_number || phone_number
    msg_parser = MessageFactory.create(user, msg['Text'], self)
    Rails.logger.debug("Received message from: #{@from}")
    return false if msg_parser.nil?
    result = msg_parser.execute
    msg = get_message(result)
    from = @from.gsub(/\A\+/,"")
    send_response(from, msg)
    result
  end

  def user_details
    {phone_number: @from}
  end

  private

  def send_response(to, msg)
    account_id = Rails.configuration.hindsightt['plivo_auth_id']
    account_token = Rails.configuration.hindsightt['plivo_auth_token']
    from = Rails.configuration.hindsightt['plivo_phone_num']
    api = RestClient.new(account_id, account_token)
    begin
      response = api.messages.create(from,[to],msg)
      Rails.logger.debug("Response from Plivo: #{response.inspect}")
    rescue PlivoRESTError => e
      Rails.logger.error("Error from Plivo: #{e.message}")
    end
  end

  def valid?(request)
    uri = request.original_url
    nonce = request.headers['HTTP_X_PLIVO_SIGNATURE_V2_NONCE']
    signature = request.headers['HTTP_X_PLIVO_SIGNATURE_V2']
    auth_token = Rails.configuration.hindsightt['plivo_auth_token']
    Plivo::Utils.valid_signature?(uri, nonce, signature, auth_token)
  end
end
