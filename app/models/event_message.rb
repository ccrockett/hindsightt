# frozen_string_literal: true

class EventMessage
  attr_reader :from
  attr_reader :username
  attr_reader :tags
  attr_reader :fields

  def initialize(message, user)
    @fields = {}
    @tags = []
    @user = user
    # Request Body parsing
    body = message.strip.gsub(/,\s+/, ',')
    msg_split = body.split(' ')
    parse_message(msg_split)
  end

  def execute
    Rails.logger.debug('User requesting a new event')
    event = Event.new(user: @user)
    event.fields = @fields
    event.tags = @tags
    event.save
  end

  private
    def parse_message(msg_split)
      msg_split.each do |msg|
        tag_split = msg.split(',')
        if tag_split.length == 1
          @tags.push(msg.downcase.tr('-', ' ').tr('.', ' '))
        else
          tag_index = 0
          while tag_index < tag_split.length  do
            if !tag_split[tag_index].nil? && !tag_split[tag_index + 1].nil?
              field_name = tag_split[tag_index].downcase
              field_value = tag_split[tag_index + 1].downcase.tr('-', ' ').tr('.', ' ')
              @fields[field_name] = field_value
            end
            tag_index += 2
          end
        end
      end
    end
end
