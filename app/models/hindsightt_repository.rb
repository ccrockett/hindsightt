# frozen_string_literal: true

class HindsighttRepository
  include Elasticsearch::Persistence::Repository
  client Elasticsearch::Client.new(host: Rails.configuration.hindsightt['es_url'], log: false)
end
