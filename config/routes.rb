# frozen_string_literal: true

Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  post '/messages/provider/:provider_name(/:url_uuid)', to: 'parse#create',
                                                        constraints: {
                                                          provider_name: Rails.configuration.hindsightt['msg_parsers'].join('|'),
                                                          url_uuid: Rails.configuration.hindsightt['url_unique']
                                                        }
  root to: proc { [200, {}, ['']] }
end
